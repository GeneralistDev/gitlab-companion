//
//  Todo.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 6/11/21.
//

import Foundation

struct Todo: Hashable, Codable, Identifiable {
    var id: Int
    var author: TodoAuthor
    var action_name: ActionName
    var target_type: TargetType
    var target: TodoTarget
    var target_url: String
    var body: String
    var state: State
    var created_at: Date
    var updated_at: Date
    
    var friendlyTitle: String {
        switch(self.target_type) {
        case .Issue:
            return "\(self.action_name.description) on Issue #\(self.target.iid): \(self.target.title)"
        case .Epic:
            return "\(self.action_name.description) on Epic &\(self.target.iid): \(self.target.title)"
        case .MergeRequest:
            return "\(self.action_name.description) on Merge Request \(self.target.iid)"
        case .AlertManagementAlert:
            return "\(self.action_name.description) on \(self.target_type.rawValue)"
        case .DesignManagementDesign:
            return "\(self.action_name.description) on \(self.target_type.rawValue)"
        }
    }
    
    enum ActionName: String, Codable {
        case DirectlyAddressed = "directly_addressed"
        case Assigned = "assigned"
        case Mentioned = "mentioned"
        case BuildFailed = "build_failed"
        case Marked  = "marked"
        case ApprovalRequired = "approval_required"
        case Unmergeable = "unmergeable"
        case MergeTrainRemoved = "merge_train_removed"
        case ReviewRequested = "review_requested"
        
        var description : String {
            switch self {
            case .DirectlyAddressed:
                return "You were directly addressed"
            case .Assigned:
                return "You were assigned"
            case .Mentioned:
                return "You were mentioned"
            case .BuildFailed:
                return "Build failed"
            case .Marked:
                return "Marked"
            case .ApprovalRequired:
                return "Your approval is required"
            case .Unmergeable:
                return "Unmergeable"
            case .MergeTrainRemoved:
                return "Merge Train Removed"
            case .ReviewRequested:
                return "Your review was requested"
            }
        }
    }

    enum TargetType: String, Codable {
        case Issue = "Issue"
        case MergeRequest = "MergeRequest"
        case DesignManagementDesign = "DesignManagement::Design"
        case AlertManagementAlert = "AlertManagement::Alert"
        case Epic = "Epic"
    }

    enum State: String, Codable {
        case Pending = "pending"
        case Done = "done"
    }
    
    struct TodoTarget: Hashable, Codable, Identifiable {
        var id: Int
        var iid: Int
        var title: String
    }
    
    struct TodoAuthor: Hashable, Codable, Identifiable {
        var id: Int
        var name: String
        var username: String
        var avatar_url: String
    }
}
