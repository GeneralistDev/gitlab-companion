//
//  GitLab_CompanionApp.swift
//  GitLab Companion
//
//  Created by Daniel Parker on 14/11/21.
//

import SwiftUI
import WatchConnectivity

@main
struct GitLab_CompanionApp: App {
    var model = ViewModelPhone()
    
    var body: some Scene {
        WindowGroup {
            ContentView(model: model)
        }
    }
}
