//
//  ViewModelPhone.swift
//  GitLab Companion
//
//  Created by Daniel Parker on 14/11/21.
//

import Foundation
import WatchConnectivity

class ViewModelPhone: NSObject, WCSessionDelegate, ObservableObject {
    @Published var session: WCSession
    
    init(session: WCSession = .default){
        self.session = session
        super.init()
        
        self.session.delegate = self
        session.activate()
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("Is reachable: \(self.session.isReachable)")
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("Session inactive")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("Session deactivated")
    }
}
