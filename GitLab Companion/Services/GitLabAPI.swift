//
//  GitLabAPI.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 6/11/21.
//

import Foundation
import SwiftUI

class GitLabAPI: ObservableObject {
    @AppStorage("PAT") private var personalAccessToken = ""
    @Published var todos = [Todo]()
    
    func loadTodos(completion: @escaping ([Todo]) -> ()) {
        guard personalAccessToken.count > 0 else {
            print ("Personal access token is empty")
            return
        }
        
        guard let url = URL(string: "https://gitlab.com/api/v4/todos") else {
            print("Invalid URL...")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue(personalAccessToken, forHTTPHeaderField: "PRIVATE-TOKEN")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            print(error as Any)
            print(response as Any)
            let decoder = JSONDecoder()
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.calendar = Calendar(identifier: .iso8601)
            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            decoder.dateDecodingStrategy = .formatted(dateFormatter)
            let todos = try! decoder.decode([Todo].self, from: data!)
            DispatchQueue.main.async {
                completion(todos)
            }
        }.resume()
    }
    
    func setTodoDone(id: Int, completion: @escaping () -> ()) {
        guard let url = URL(string: "https://gitlab.com/api/v4/todos/\(id)/mark_as_done") else {
            print("Invalid URL...")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue(personalAccessToken, forHTTPHeaderField: "PRIVATE-TOKEN")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            print(error as Any)
            print(response as Any)
            
            DispatchQueue.main.async {
                completion()
            }
        }.resume()
    }
}
