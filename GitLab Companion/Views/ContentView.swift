//
//  ContentView.swift
//  GitLab Companion
//
//  Created by Daniel Parker on 14/11/21.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var model: ViewModelPhone
    @State var reachable = "No"
    @State var messageText = ""
    
    var body: some View {
        VStack {
            Text("Reachable \(reachable)")
            
            Button(action: {
                self.model.session.activate()
                
                if self.model.session.isReachable {
                    self.reachable = "Yes"
                }
                else {
                    self.reachable = "No"
                }
            }) {
                Text("Update")
            }
            
            TextField("Input your message", text: $messageText)
            Button(action: {
                self.model.session.sendMessage(["message": self.messageText], replyHandler: nil) { (error) in
                    print(error.localizedDescription)
                }
            }) {
                Text("Send Message")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let model = ViewModelPhone()
        ContentView(model: model)
    }
}
