//
//  TodoRow.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 6/11/21.
//

import SwiftUI

struct TodoRow: View {
    var todo: Todo
    
    let dateFormatter: DateFormatter
    
    init(todo: Todo) {
        self.todo = todo
        dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
    }
    
    var body: some View {
        VStack {
            HStack {
                Text(todo.created_at, formatter: dateFormatter)
                    .font(.footnote)
                    .bold()
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
                
                
            
            HStack {
                TodoAuthorImage(todo: todo)
                    .frame(width: 35, height: 35)
                
                Text(todo.body)
                    .lineLimit(2)
                    .font(.subheadline)
                    .truncationMode(.tail)
            }.frame(maxWidth: .infinity, alignment: .leading)
        }
    }
}

struct TodoRow_Previews: PreviewProvider {
    static var previews: some View {
        TodoRow(todo: todos[11])
    }
}
