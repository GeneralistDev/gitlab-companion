//
//  ClearTokenButton.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 7/11/21.
//

import SwiftUI

struct ClearTokenButton: View {
    @AppStorage("PAT") private var personalAccessToken = ""
    @State private var isConfirming = false
    
    var body: some View {
        Button("Clear Token") {
            isConfirming = true
        }
        .confirmationDialog(
            "Are you sure you want to clear the GitLab token?",
            isPresented: $isConfirming
        ) {
            Button("Yes") {
                personalAccessToken = ""
            }.frame(height: 40)
            
            Button("Cancel", role: .cancel) {}.frame(height: 40)
        }.frame(height: 40)
    }
}

struct ClearTokenButton_Previews: PreviewProvider {
    static var previews: some View {
        ClearTokenButton()
    }
}
