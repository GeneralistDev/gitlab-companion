//
//  TodoAuthorImage.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 6/11/21.
//

import SwiftUI
import Kingfisher

let placeholderImage = Image(systemName: "person.crop.circle.fill")
    

struct TodoAuthorImage: View {
    var todo: Todo

    var body: some View {
        KFImage.url(URL(string: todo.author.avatar_url))
            .placeholder {
                placeholderImage
                    .resizable()
                    .scaledToFit()
                    .clipShape(Circle())
            }
            .resizable()
            .scaledToFit()
            .clipShape(Circle())
    }
}

struct TodoAuthorImage_Previews: PreviewProvider {
    static var previews: some View {
        TodoAuthorImage(todo: todos[0])
    }
}
