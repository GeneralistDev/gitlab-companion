//
//  TodoDetail.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 6/11/21.
//

import SwiftUI

struct TodoDetail: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.openURL) var openURL
    var todo: Todo
    let dateFormatter: DateFormatter
    
    init(todo: Todo) {
        self.todo = todo
        dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
    }
    
    var body: some View {
        ScrollView {
            Text(todo.created_at, formatter: self.dateFormatter)
                .font(.footnote)
                .bold()
                .frame(maxWidth: .infinity, alignment: .leading)
            
            HStack {
                TodoAuthorImage(todo: todo)
                    .frame(width: 35, height: 35)
                    .padding(.leading, 0)
                
                VStack {
                    Text(todo.author.name)
                    Text("@\(todo.author.username)")
                }
            }.frame(maxWidth: .infinity, alignment: .leading)
            
            HStack {
                Text("\(todo.friendlyTitle)")
                    .bold()
            }.frame(maxWidth: .infinity, alignment: .leading)
            
            
            Divider()
            
            Text(todo.body)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            Divider()
            
            Button(action: {
                guard let url = URL(string: todo.target_url) else {
                    print("Invalid URL")
                    return
                }

                openURL(url)
            }) {
                Text("Open Link")
            }
//
//            Link(destination: URL(string: todo.target_url)!) {
//                Text("Open Link")
//            }
            
            Button(action: {
                GitLabAPI().setTodoDone(id: todo.id) {
                    presentationMode.wrappedValue.dismiss()
                }
            }) {
                Text("Mark as Done")
            }
        }.frame(maxWidth: .infinity, alignment: .leading)
        .padding(.top)
    }
}

struct TodoDetail_Previews: PreviewProvider {
    static var previews: some View {
        TodoDetail(todo: todos[1])
    }
}
