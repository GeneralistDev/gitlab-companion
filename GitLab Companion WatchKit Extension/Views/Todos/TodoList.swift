//
//  TodoList.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 6/11/21.
//

import SwiftUI

struct TodoList: View {
    @State var todos = [Todo]()
    @State var loading = false
    
    var body: some View {
        NavigationView {
            if todos.count > 0 {
                List {
                    Button(action: {
                        triggerRefresh()
                    }, label: {
                        if loading {
                            ProgressView()
                        } else {
                            Text("Refresh")
                        }
                    })
                    
                    ForEach(todos) { todo in
                        NavigationLink(destination: TodoDetail(todo: todo).onDisappear {
                            triggerRefresh()
                        }) {
                            TodoRow(todo: todo)
                        }
                    }
                    
                    ClearTokenButton()
                }
                .navigationTitle("Todos")
            }
            
            if self.todos.count == 0 {
                ScrollView {
                    Text("""
                         🎉🎉
                         You've reached inbox zero!!
                         🎉🎉
                         """)
                        .multilineTextAlignment(.center)
                        .padding()
                        .frame(maxWidth: .infinity, alignment: .center)
                    
                    Button(action: {
                        triggerRefresh()
                    }, label: {
                        if loading {
                            ProgressView()
                        } else {
                            Text("Refresh")
                        }
                    }).frame(height: 40)
                    
                    ClearTokenButton()
                }
                .navigationTitle("Todos")
                .padding()
            }
        }
        .onAppear() {
            triggerRefresh()
        }
    }
    
    func triggerRefresh() {
        if (!self.loading) {
            self.loading = true

            GitLabAPI().loadTodos { (todos) in
                self.todos = todos
                self.loading = false
            }
            
        }
    }
}

struct TodoList_Previews: PreviewProvider {
    static var previews: some View {
        TodoList(todos: todos)
        
        TodoList(todos: [Todo]())
    }
}
