//
//  ContentView.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 6/11/21.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var model: ViewModelWatch
    @AppStorage("PAT") private var personalAccessToken = ""
    
    var body: some View {
        VStack {
            Text(self.model.messageText)
            
            if personalAccessToken.count == 0 {
                Text("Enter your gitlab.com personal access token with 'api' scope")
                
                Divider()
                
                TextField(
                    "Personal Access Token",
                    text: $personalAccessToken
                )
                    .disableAutocorrection(true)
                    .textInputAutocapitalization(.never)
            } else {
                TodoList()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let model = ViewModelWatch()
        ContentView(model: model)
    }
}
