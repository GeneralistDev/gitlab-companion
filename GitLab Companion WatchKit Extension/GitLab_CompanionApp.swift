//
//  GitLab_CompanionApp.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 14/11/21.
//

import SwiftUI

@main
struct GitLab_CompanionApp: App {
    @ObservedObject var model = ViewModelWatch()
    
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView(model: model)
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
