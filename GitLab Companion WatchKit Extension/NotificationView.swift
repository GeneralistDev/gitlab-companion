//
//  NotificationView.swift
//  GitLab Companion WatchKit Extension
//
//  Created by Daniel Parker on 14/11/21.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
